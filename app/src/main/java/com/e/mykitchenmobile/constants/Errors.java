package com.e.mykitchenmobile.constants;

public enum Errors {
    DataError,
    AutorizationError
}
