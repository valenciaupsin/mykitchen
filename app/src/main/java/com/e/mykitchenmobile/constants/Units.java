package com.e.mykitchenmobile.constants;

public enum Units {
    error,
    gramos,
    piezas,
    mililitros,
    grs,
    pzs,
    mls
}
