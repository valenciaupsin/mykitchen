package com.e.mykitchenmobile.constants;

public enum DificultyID {
    Facil,
    Intermedio,
    Difícil
}
