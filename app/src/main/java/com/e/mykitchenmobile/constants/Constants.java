package com.e.mykitchenmobile.constants;

public enum Constants {
    NoTags,
    NoRecipesTag,
    NoRecipesName,
    NoRecipesIngredient,
    NoProducts,
    Connection_Fail,
    Add,
    Remove,
    Replace

}
