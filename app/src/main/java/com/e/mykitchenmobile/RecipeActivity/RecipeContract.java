package com.e.mykitchenmobile.RecipeActivity;


import com.e.mykitchenmobile.APIServices.response.RecipeDetailResponse;
import com.e.mykitchenmobile.RecipeActivity.model.RecipeDetail;

import retrofit2.Response;

public interface RecipeContract {
    interface View{
        void triggerGetRecipeDetails(String recipeId);

        void setRecipesRetails(RecipeDetail recipeDetails);
    }
    interface Presenter{
        void requestRecipeDetails(String recipeId);

        void triggerSetRecipeDetails(Response<RecipeDetailResponse> response);

    }
    interface Interactor{
        void getRecipeDetail(String recipeId);

    }
    interface Router{

    }
}
