package com.e.mykitchenmobile.RecipeActivity.model;

import com.e.mykitchenmobile.ExplorarActivity.model.Product;

public class Ingredient {
    private String id;
    private String productId;
    private int quantity;
    private Product product;
    private String unitId;

    public Ingredient() {
    }

    public String getId() {
        return id;
    }

    public String getProductId() {
        return productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public Product getProduct() {
        return product;
    }
}
