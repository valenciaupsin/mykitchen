package com.e.mykitchenmobile.APIServices.response;


import com.e.mykitchenmobile.inventory.models.StockText;

public class GenericResponse {
    private String details;
    private StockText content;

    public String getDetails() {
        return details;
    }

    public StockText getContent() {
        return content;
    }
}
