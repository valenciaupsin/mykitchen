package com.e.mykitchenmobile.APIServices.response;


import com.e.mykitchenmobile.ExplorarActivity.model.ProductCategory;

import java.util.ArrayList;

public class ProductCategoryResponse {
    private String details;
    private ArrayList<ProductCategory> content;

    public String getDetails() {
        return details;
    }

    public ArrayList<ProductCategory> getContent() {
        return content;
    }
}
