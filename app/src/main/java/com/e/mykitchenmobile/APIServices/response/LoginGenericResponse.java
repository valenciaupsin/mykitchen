package com.e.mykitchenmobile.APIServices.response;

public class LoginGenericResponse {
    private String detail;
    private LoginResponse content;

    public String getDetail() {
        return detail;
    }

    public LoginResponse getContent() {
        return content;
    }
}
