package com.e.mykitchenmobile.APIServices.response;


import com.e.mykitchenmobile.RegistroActivity.model.NewUser;

public class SingUpGenericResponse {
    private String details;
    private NewUser content;

    public String getDetails() {
        return details;
    }

    public NewUser getContent() {
        return content;
    }
}
