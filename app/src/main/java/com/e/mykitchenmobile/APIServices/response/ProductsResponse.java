package com.e.mykitchenmobile.APIServices.response;


import com.e.mykitchenmobile.ExplorarActivity.model.Product;

import java.util.ArrayList;

public class ProductsResponse {
    private String details;
    private ArrayList<Product> content;

    public String getDetails() {
        return details;
    }

    public ArrayList<Product> getContent() {
        return content;
    }
}
