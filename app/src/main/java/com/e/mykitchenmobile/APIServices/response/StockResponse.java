package com.e.mykitchenmobile.APIServices.response;


import com.e.mykitchenmobile.inventory.models.Stock;

import java.util.ArrayList;

public class StockResponse {
    private String details;
    private ArrayList<Stock> content;

    public String getDetails() {
        return details;
    }

    public ArrayList<Stock> getContent() {
        return content;
    }
}
