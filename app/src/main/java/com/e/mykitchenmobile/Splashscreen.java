package com.e.mykitchenmobile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.e.mykitchenmobile.ExplorarActivity.ExplorarActivity;

public class Splashscreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        new Handler().postDelayed(() -> {
            Intent i = new Intent(Splashscreen.this, ExplorarActivity.class);
            startActivity(i);
            // close this activity
            finish();
        }, 1000); // wait for 1 second
    }
}
