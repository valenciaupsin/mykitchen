package com.e.mykitchenmobile.inventory.models;

public class NewProduct {
    private String productId;
    private String subproductId;
    private int quantity;

    public NewProduct(String productId, String subproductId, int quantity) {
        this.productId = productId;
        this.subproductId = subproductId;
        this.quantity = quantity;
    }

    public String getProductId() {
        return productId;
    }

    public String getSubproductId() {
        return subproductId;
    }

    public int getCant() {
        return quantity;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public void setSubproductId(String subproductId) {
        this.subproductId = subproductId;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}

