package com.e.mykitchenmobile.inventory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.e.mykitchenmobile.R;
import com.e.mykitchenmobile.inventory.models.Stock;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DeleteProductAdapter extends RecyclerView.Adapter<DeleteProductAdapter.ViewHolder> {
    private Context context;
    private ArrayList<Stock> productArrayList;
    private OnItemClickListener nListener;


    public DeleteProductAdapter(Context context, ArrayList<Stock> productArrayList) {
        this.context = context;
        this.productArrayList = productArrayList;
    }
    public interface  OnItemClickListener{
        void onDeleteClick(int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener){
        nListener = listener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ingred_item, null,false);
        return new ViewHolder(view,nListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso.with(context).load("https://s3.amazonaws.com/images.mykitchen.com.mx/products/"+productArrayList.get(position).getProductId().getImage())
                .into(holder.imageView);
        holder.name.setText(productArrayList.get(position).getProductId().getName()+ " "+ productArrayList.get(position).getSubproductId().getPresentation());
        holder.quantity.setVisibility(View.GONE);
        holder.unit.setVisibility(View.GONE);
        holder.menosBtn.setVisibility(View.GONE);
        holder.masBtn.setVisibility(View.GONE);
        holder.deleteBtn.setVisibility(View.VISIBLE);

    }

    @Override
    public int getItemCount() {
        return productArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView name;
        LinearLayout buttonsLayot;
        ImageButton menosBtn;
        TextView quantity;
        ImageButton masBtn;
        TextView unit;
        ImageButton deleteBtn;

        public ViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageIngre_iv);
            name = itemView.findViewById(R.id.nombreIngre_tv);
            buttonsLayot = itemView.findViewById(R.id.quantity_btns);
            menosBtn = itemView.findViewById(R.id.menosIngre_btn);
            quantity = itemView.findViewById(R.id.cantidadIngre_tv);
            masBtn = itemView.findViewById(R.id.masIngre_btn);
            deleteBtn = itemView.findViewById(R.id.deleteIng_btn);
            unit = itemView.findViewById(R.id.unidadIngre_tv);

            deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listener.onDeleteClick(position);
                        }
                    }
                }
            });
        }
    }
}
