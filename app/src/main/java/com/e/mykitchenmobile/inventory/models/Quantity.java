package com.e.mykitchenmobile.inventory.models;

public class Quantity {
    private String quantity;

    public Quantity(String quantity) {
        this.quantity = quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
