package com.e.mykitchenmobile.inventory.models;

public enum operations {
    watch,
    add,
    delete,
    modify
}
