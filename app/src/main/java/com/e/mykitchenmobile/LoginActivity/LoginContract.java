package com.e.mykitchenmobile.LoginActivity;

import com.e.mykitchenmobile.APIServices.response.LoginGenericResponse;
import com.e.mykitchenmobile.constants.Errors;


import retrofit2.Response;

public interface LoginContract {

    interface View{

        void onError(String error);

        void loginSuccess(String token, String kitchenId, String accountId);
    }
    interface Presenter{
        void triggerLogin(String email, String password);

        void triggerLoginResponse(Response<LoginGenericResponse> response);

        void triggerError(Errors dataError);
    }
    interface Interactor{
        void login(String email, String password);
    }

}
