package com.e.mykitchenmobile.ExplorarActivity;

import android.content.Context;
import android.content.Intent;

import com.e.mykitchenmobile.ExplorarActivity.ExplorarContract;
import com.e.mykitchenmobile.RecipeActivity.RecipeActivity;
import com.e.mykitchenmobile.RegistroActivity.RegistroActivity;

public class ExplorarRouter implements ExplorarContract.Router {
    private ExplorarContract.Presenter presenter;
    public ExplorarRouter(ExplorarPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void toSingUp(Context context) {
        Intent intent = new Intent(context, RegistroActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void toRecipeDetails(Context context, String recipeId) {
        Intent intent = new Intent(context, RecipeActivity.class);
        intent.putExtra("RECIPE_ID", recipeId);
        context.startActivity(intent);
    }
}
