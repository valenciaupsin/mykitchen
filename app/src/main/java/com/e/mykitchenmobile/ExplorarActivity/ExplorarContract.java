package com.e.mykitchenmobile.ExplorarActivity;

import android.content.Context;

import com.e.mykitchenmobile.APIServices.response.ProductCategoryResponse;
import com.e.mykitchenmobile.APIServices.response.RecipesResponse;
import com.e.mykitchenmobile.APIServices.response.TagResponse;
import com.e.mykitchenmobile.ExplorarActivity.model.ProductCategory;
import com.e.mykitchenmobile.ExplorarActivity.model.RecipeShort;
import com.e.mykitchenmobile.ExplorarActivity.model.Tag;
import com.e.mykitchenmobile.constants.Constants;

import java.util.ArrayList;

public interface ExplorarContract {

    interface View{

        void triggerRequestProductsCategories();

        void triggerRequestRecipesByTag(String tag);

        void triggerRequestRecipesByProducts(String products);

        void sendTags(ArrayList<Tag> tagsList);

        void sendProductsCategories(ArrayList<ProductCategory> content);

        void sendRecipes(ArrayList<RecipeShort> recipesList);

        void triggerGoToRecipeDetails(String recipeId);

        void onRecipesTagError();

        void onConnectionError();

        void onRecipesNameError();

        void onRecipesIngredientsError();

    }

    interface TagsFragment {

        void setTags(ArrayList<Tag> tagsList);

    }

    interface Presenter{

        void requestTags();

        void requestRecipesByTag(String tag);

        void requestRecipesByIngredients(String products);

        void requestRecipesByStock(String TOKEN);

        void requestProductsCategories();

        void triggerSetTags(TagResponse tagResponse);

        void triggerSetRecipes(RecipesResponse recipesResponse);

        void triggerSetError(Constants error);

        void triggerSetProductCategories(ProductCategoryResponse productCategoryResponse);

        void goToRecipeDetails(Context context, String recipeId);

    }
    interface Interactor{

        void getTags();

        void getRecipesByTag(String tag);


        void getRecipesByIngredients(String ingredients);

        void getProductCategoriesComplete();

        void getRecipesByStock(String TOKEN);

    }

    interface Router{

        void toSingUp(Context context);

        void toRecipeDetails(Context context, String recipeId);

    }

}
