package com.e.mykitchenmobile.ExplorarActivity.model;

public class RecipeShort {
    private String id;
    private String name;
    private String image;
    private String time;
    private String calories;
    private String difficultyId;
    private int total_ingredients;
    private int total_owned;
    private String percentage;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDifficultyId() {
        return difficultyId;
    }

    public String getTime() {
        return time;
    }

    public String getCalories() {
        return calories;
    }

    public int getTotal_ingredients() {
        return total_ingredients;
    }

    public int getTotal_owned() {
        return total_owned;
    }
}
