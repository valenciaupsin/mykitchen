package com.e.mykitchenmobile.ExplorarActivity;

import android.content.Context;

import com.e.mykitchenmobile.APIServices.response.ProductCategoryResponse;
import com.e.mykitchenmobile.APIServices.response.RecipesResponse;
import com.e.mykitchenmobile.APIServices.response.TagResponse;
import com.e.mykitchenmobile.constants.Constants;

public class ExplorarPresenter implements ExplorarContract.Presenter {

    private ExplorarContract.Interactor interactor;
    private ExplorarContract.View view;
    private ExplorarContract.Router router;

    ExplorarPresenter(ExplorarActivity view) {
        this.view = view;
        interactor= new ExplorarInteractor(this);
        router = new ExplorarRouter(this);
    }

    @Override
    public void requestTags() {
        interactor.getTags();
    }

    @Override
    public void requestRecipesByTag(String tag) {
        interactor.getRecipesByTag(tag);
    }

    @Override
    public void requestRecipesByIngredients(String products) {
        interactor.getRecipesByIngredients(products);
    }

    @Override
    public void requestRecipesByStock(String TOKEN) {
        interactor.getRecipesByStock(TOKEN);
    }

    @Override
    public void requestProductsCategories() {
        interactor.getProductCategoriesComplete();
    }

    @Override
    public void triggerSetTags(TagResponse tagResponse) {
        if (tagResponse.getContent() != null) {
            view.sendTags(tagResponse.getContent());}
        else {
            triggerSetError(Constants.NoTags);
        }
    }

    @Override
    public void triggerSetRecipes(RecipesResponse recipesResponse) {
        if (recipesResponse.getContent().size() != 0) {
            view.sendRecipes(recipesResponse.getContent());}
        else {
            triggerSetError(Constants.NoRecipesTag);
        }
    }

    @Override
    public void triggerSetError(Constants error) {
        switch (error){
            case NoRecipesTag:
                view.onRecipesTagError();
                break;
            case NoRecipesName:
                view.onRecipesNameError();
                break;
            case NoRecipesIngredient:
                view.onRecipesIngredientsError();
                break;
            case Connection_Fail:
                view.onConnectionError();
                break;
        }
    }

    @Override
    public void triggerSetProductCategories(ProductCategoryResponse productCategoryResponse) {
        view.sendProductsCategories(productCategoryResponse.getContent());
    }

    @Override
    public void goToRecipeDetails(Context context, String recipeId) {
        router.toRecipeDetails(context, recipeId);
    }
}
