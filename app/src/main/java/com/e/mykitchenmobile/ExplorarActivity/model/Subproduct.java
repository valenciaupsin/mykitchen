package com.e.mykitchenmobile.ExplorarActivity.model;

public class Subproduct {
    private String id = "";
    private String productId = "";
    private String presentation = "";
    private String equivalence = "";
    private String unitId = "";
    private int step = 0;
    private String stepUnitId = "";
    private String negligible = "";
    private String expiration = "";
    private String createdAt = "";
    private String updatedAt = "";
    private int quantity =0;


    public String getId() {
        return id;
    }

    public String getProductId() {
        return productId;
    }

    public String getPresentation() {
        return presentation;
    }

    public String getEquivalence() {
        return equivalence;
    }

    public String getUnitId() {
        return unitId;
    }

    public int getStep() {
        return step;
    }

    public String getStepUnitId() {
        return stepUnitId;
    }

    public String getNegligible() {
        return negligible;
    }

    public String getExpiration() {
        return expiration;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public int getQuantity() {
        return quantity;
    }
    public void minus(int quantity){
        if (this.quantity < quantity)
            this.quantity = 0;
        else
            this.quantity -= quantity;
    }
    public void plus(int quantity){
        this.quantity += quantity;
    }

    public void delete(){
        this.quantity = 0;
    }
}
