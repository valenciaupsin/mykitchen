package com.e.mykitchenmobile.ExplorarActivity.tagsFragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.e.mykitchenmobile.ExplorarActivity.ExplorarActivity;
import com.e.mykitchenmobile.ExplorarActivity.ExplorarContract;
import com.e.mykitchenmobile.ExplorarActivity.model.Tag;
import com.e.mykitchenmobile.R;

import java.util.ArrayList;

public class TagsFragment extends Fragment implements ExplorarContract.TagsFragment{

    private GridView gridView;
    private ProgressBar progressBar;
    private View view;
    private ExplorarContract.View activity;

    public TagsFragment() {
        // Required empty public constructor
    }

    public TagsFragment(ExplorarActivity activity){
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tags, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        initComponents();
    }

    private void initComponents() {
        gridView = view.findViewById(R.id.gridView);
        progressBar = view.findViewById(R.id.tagsProgressBar);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setTags(ArrayList<Tag> tagsList) {
        progressBar.setVisibility(View.GONE);
        TagAdapter adapter = new TagAdapter(getContext(), tagsList);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(this::onClick);
    }

    //CONTROLA CLICKS DE TAGS, AL CLICKEAR UNA SE MANDA EL TAG
    private void onClick(AdapterView<?> adapterView, View view, int i, long l) {
        String tag = String.valueOf(((Tag) gridView.getAdapter().getItem(i)).getId());
        activity.triggerRequestRecipesByTag(tag);
    }

    private void toast(String mensaje){
        Toast.makeText(getContext(),mensaje,Toast.LENGTH_SHORT).show();
    }
}
