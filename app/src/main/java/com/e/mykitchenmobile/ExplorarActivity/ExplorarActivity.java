package com.e.mykitchenmobile.ExplorarActivity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.e.mykitchenmobile.ExplorarActivity.model.Product;
import com.e.mykitchenmobile.ExplorarActivity.model.ProductCategory;
import com.e.mykitchenmobile.ExplorarActivity.model.RecipeShort;
import com.e.mykitchenmobile.ExplorarActivity.model.Tag;
import com.e.mykitchenmobile.ExplorarActivity.productsFragment.ProductsFragment;
import com.e.mykitchenmobile.ExplorarActivity.recipesFragment.RecipesFragment;
import com.e.mykitchenmobile.ExplorarActivity.tagsFragment.TagsFragment;
import com.e.mykitchenmobile.LoginActivity.LoginActivity;
import com.e.mykitchenmobile.R;
import com.e.mykitchenmobile.constants.Constants;
import com.e.mykitchenmobile.inventory.InventoryActivity;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.e.mykitchenmobile.constants.Constants.Add;
import static com.e.mykitchenmobile.constants.Constants.Replace;

public class ExplorarActivity extends AppCompatActivity implements ExplorarContract.View{

    private String USERMODE = "GUEST";
    private String TOKEN;
    private String KITCHENID;
    private String ACCOUNTID;
    private Toolbar toolbar;
    private NavigationView navigationView;
    private ImageButton imageButton;
    private DrawerLayout drawerLayout;
    private ExplorarContract.Presenter presenter;
    private ExplorarContract.TagsFragment tagsFragment;
    private ChipGroup chipGroup;
    private Chip tagsChip;
    private Chip ingredientsChip;
    private Chip inventoryChip;

    FragmentManager fragmentManager = getSupportFragmentManager();
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explorar);
        presenter = new ExplorarPresenter(this);
        tagsFragment = new TagsFragment(this);
        initComponents();
        loadFragment((Fragment) tagsFragment, Add);
        triggerRequestGetTags();

    }

    @Override
    protected void onResume() {
        super.onResume();
        sharedPref = getPreferences(Context.MODE_PRIVATE);
        USERMODE = sharedPref.getString("USERMODE", "GUEST");
        TOKEN = sharedPref.getString("TOKEN", "");
        KITCHENID = sharedPref.getString("KITCHENID", "");
        ACCOUNTID = sharedPref.getString("ACCOUNTID", "");
        if (USERMODE.equals("GUEST")){
            inventoryChip.setVisibility(View.INVISIBLE);
        }

    }



    public void initComponents(){
        sharedPref = getPreferences(Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        navigationView = findViewById(R.id.nav_view);
        if (navigationView != null) { setupDrawerContent(navigationView); }
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout= findViewById(R.id.drawer_layout);

        imageButton = findViewById(R.id.menuButton);
        imageButton.setOnClickListener(this::onClick);

        chipGroup = findViewById(R.id.chipGroup);

        tagsChip = findViewById(R.id.tagsChip);
        tagsChip.setOnClickListener(this::onClick);

        ingredientsChip = findViewById(R.id.ingredientsChip);
        ingredientsChip.setOnClickListener(this::onClick);

        inventoryChip = findViewById(R.id.inventoryChip);
        inventoryChip.setOnClickListener(this::onClick);
    }

    public void onClick(View view) {
        if (view == imageButton){
            drawerLayout.openDrawer(GravityCompat.START);
        }else if (view == tagsChip){
            tagsChip.setChecked(true);
            tagsFragment = new TagsFragment(this);
            loadFragment((Fragment)tagsFragment, Replace);
            triggerRequestGetTags();
        }else if (view == ingredientsChip){
            ingredientsChip.setChecked(true);
            triggerRequestProductsCategories();
        }else if (view == inventoryChip){
            inventoryChip.setChecked(true);
            presenter.requestRecipesByStock(TOKEN);
        }
    }

    void loadFragment(Fragment newFragment, Constants action){
        Thread thread = new Thread(() -> {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            switch (action){
                case Add:
                    fragmentTransaction.add(R.id.fragment, newFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                    break;
                case Replace:
                    fragmentTransaction.replace(R.id.fragment, newFragment);
                    fragmentTransaction.commit();
                    break;
                case Remove:
                    fragmentTransaction.remove(newFragment);
                    fragmentTransaction.commit();
                    break;
            }

        });
        thread.start();

    }

    private void triggerRequestGetTags(){
        presenter.requestTags();
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    switch (menuItem.getItemId()){
                        case R.id.nav_recetas:
                            drawerLayout.closeDrawer(GravityCompat.START);
//                            Fragment fragment = new TagsFragment();
                            //loadFragment(fragment);
                            break;
                        case R.id.nav_inventario:
                            drawerLayout.closeDrawer(GravityCompat.START);
                            if (USERMODE.equals("GUEST")){
                                Intent intent = new Intent(this, LoginActivity.class);
                                startActivityForResult(intent, 1);
                            }else{
                                Intent intent = new Intent(this, InventoryActivity.class);
                                intent.putExtra("TOKEN", TOKEN);
                                intent.putExtra("KITCHENID", KITCHENID);
                                intent.putExtra("ACCOUNTID", ACCOUNTID);
                                startActivityForResult(intent, 2);
                            }
                            break;
                        case  R.id.nav_cerrarSesion: {
                            drawerLayout.closeDrawer(GravityCompat.START);
                            if (USERMODE.equals("CLIENT")) {
                                logOut();
                                toast("Sesión cerrada");
                            }
                            else
                                toast("No hay una sesión activa apara cerrar");
                            break;
                        }
                        case R.id.nav_salir:
                            finish();
                            break;
                    }
                    return true;
                }
        );
    }private void logOut(){
        USERMODE = "GUEST";
        TOKEN = "";
        KITCHENID = "";
        ACCOUNTID = "";
        editor.putString("USERMODE", "GUEST");
        editor.putString("TOKEN", "");
        editor.putString("KITCHENID", "");
        editor.putString("ACCOUNTID", "");
        editor.apply();
        inventoryChip.setVisibility(View.INVISIBLE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == RESULT_OK){
            try {
                if (data.hasExtra("USERMODE") && data.hasExtra("TOKEN") && data.hasExtra("KITCHENID") && data.hasExtra("ACCOUNTID")) {
                    USERMODE = data.getExtras().getString("USERMODE");
                    TOKEN = data.getExtras().getString("TOKEN");
                    KITCHENID = data.getExtras().getString("KITCHENID");
                    ACCOUNTID = data.getExtras().getString("ACCOUNTID");

                    editor.putString("USERMODE", USERMODE);
                    editor.putString("TOKEN", TOKEN);
                    editor.putString("KITCHENID", KITCHENID);
                    editor.putString("ACCOUNTID", ACCOUNTID);
                    editor.apply();

                    inventoryChip.setVisibility(View.VISIBLE);
                }
            }catch (Throwable e){
                logOut();
            }

        }
    }

    @Override
    public void triggerRequestProductsCategories() {
        presenter.requestProductsCategories();
    }

    @Override
    public void triggerRequestRecipesByTag(String tag) {
        presenter.requestRecipesByTag(tag);
    }

    @Override
    public void triggerRequestRecipesByProducts(String products) {
        presenter.requestRecipesByIngredients(products);
    }

    @Override
    public void sendTags(ArrayList<Tag> tagsList) {
        tagsFragment.setTags(tagsList);
    }

    @Override
    public void sendProductsCategories(ArrayList<ProductCategory> content) {
        HashMap<ProductCategory, List<Product>> item = new HashMap<>();

        for (int x=0; x<content.size(); x++){
            ArrayList<Product> products = new ArrayList<>(content.get(x).getProduct());
            item.put(content.get(x), products);
        }

        loadFragment(new ProductsFragment(this, item),Replace);
    }

    @Override
    public void sendRecipes(ArrayList<RecipeShort> recipesList) {
        chipGroup.setVisibility(View.GONE);
        loadFragment(new RecipesFragment(this, recipesList), Add);
    }

    @Override
    public void triggerGoToRecipeDetails(String recipeId) {
        presenter.goToRecipeDetails(this, recipeId);
    }

    @Override
    public void onRecipesTagError() {
        toast(getString(R.string.recetas_etiqueta_error));
    }

    @Override
    public void onConnectionError() {
        toast(getString(R.string.error_conexion));
    }

    @Override
    public void onRecipesNameError() {
        toast(getString(R.string.recetas_nombre_error));
    }

    @Override
    public void onRecipesIngredientsError() {
        toast(getString(R.string.recetas_ingrediente_error));
    }
    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragmentView = fragmentManager.findFragmentById(R.id.fragment);
        if(fragmentView instanceof RecipesFragment) {
            fragmentTransaction.remove(fragmentView);
            fragmentTransaction.commit();
            chipGroup.setVisibility(View.VISIBLE);
        }
        super.onBackPressed();
    }
    private void toast(String mensaje){ Toast.makeText(ExplorarActivity.this,mensaje,Toast.LENGTH_SHORT).show(); }
}
