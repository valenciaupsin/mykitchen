package com.e.mykitchenmobile.ExplorarActivity;

import android.util.Log;

import com.e.mykitchenmobile.APIServices.APIAdapter;
import com.e.mykitchenmobile.APIServices.response.ProductCategoryResponse;
import com.e.mykitchenmobile.APIServices.response.RecipesResponse;
import com.e.mykitchenmobile.APIServices.response.TagResponse;
import com.e.mykitchenmobile.constants.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExplorarInteractor implements ExplorarContract.Interactor {
    ExplorarContract.Presenter presenter;

    public ExplorarInteractor(ExplorarPresenter presenter) {
        this.presenter = presenter;
    }

    private String token = "S<yus%;|ZO'1k/ISa^H+6_,!:&$0Z+kM9)B?;f`=]=p%q!)uJ^x_!F!7!LL&F|B";
    private String limit = "50";
    private String offset = "0";

    @Override
    public void getTags() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Call<TagResponse> call = APIAdapter.getApiService(token).getTagsService(token);
                call.enqueue(new Callback<TagResponse>() {
                    @Override
                    public void onResponse(Call<TagResponse> call, Response<TagResponse> response) {
                        switch (response.code()){
                            case 200: presenter.triggerSetTags(response.body()); break;
                            case 201: Log.d("RESPONSE ERROR", "Error 201"); break;
                            case 202: Log.d("RESPONSE ERROR", "Error 202"); break;
                        }
                    }

                    @Override
                    public void onFailure(Call<TagResponse> call, Throwable t) {
                        presenter.triggerSetError(Constants.Connection_Fail);
                    }
                });
            }
        });
        thread.start();
    }

    @Override
    public void getRecipesByTag(String tag) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Call<RecipesResponse> call = APIAdapter.getApiService(token).getRecipesByTag(token, tag, limit,offset);
                call.enqueue(new Callback<RecipesResponse>() {
                    @Override
                    public void onResponse(Call<RecipesResponse> call, Response<RecipesResponse> response) {
                        switch (response.code()){
                            case 200: presenter.triggerSetRecipes(response.body()); break;
                            case 201: Log.d("RESPONSE ERROR", "Error 201"); break;
                            case 202: Log.d("RESPONSE ERROR", "Error 202"); break;
                        }
                    }

                    @Override
                    public void onFailure(Call<RecipesResponse> call, Throwable t) {
                        presenter.triggerSetError(Constants.Connection_Fail);
                    }
                });
            }
        });
        thread.start();
    }


    @Override
    public void getRecipesByIngredients(String ingredients) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Call<RecipesResponse> call = APIAdapter.getApiService(token).getRecipesByIngredients(token,ingredients,limit, offset);
                call.enqueue(new Callback<RecipesResponse>() {
                    @Override
                    public void onResponse(Call<RecipesResponse> call, Response<RecipesResponse> response) {
                        switch (response.code()){
                            case 200: presenter.triggerSetRecipes(response.body()); break;
                            case 201: Log.d("RESPONSE ERROR", "Error 201"); break;
                            case 202: Log.d("RESPONSE ERROR", "Error 202"); break;
                        }
                    }

                    @Override
                    public void onFailure(Call<RecipesResponse> call, Throwable t) {
                        presenter.triggerSetError(Constants.Connection_Fail);
                    }
                });
            }
        });
        thread.start();
    }

    @Override
    public void getProductCategoriesComplete() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Call<ProductCategoryResponse> call = APIAdapter.getApiService(token).getProductsCategoriesComplete(token);
                call.enqueue(new Callback<ProductCategoryResponse>() {
                    @Override
                    public void onResponse(Call<ProductCategoryResponse> call, Response<ProductCategoryResponse> response) {
                        switch (response.code()){
                            case 200: presenter.triggerSetProductCategories(response.body());break;
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductCategoryResponse> call, Throwable t) {
                        presenter.triggerSetError(Constants.NoProducts);
                    }
                });
            }
        });
        thread.start();
    }

    @Override
    public void getRecipesByStock(String TOKEN) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Call<RecipesResponse> call = APIAdapter.getApiService2(TOKEN).getRecipesByStock(TOKEN, limit,offset);
                call.enqueue(new Callback<RecipesResponse>() {
                    @Override
                    public void onResponse(Call<RecipesResponse> call, Response<RecipesResponse> response) {
                        switch (response.code()){
                            case 200: presenter.triggerSetRecipes(response.body()); break;
                            case 201: Log.d("RESPONSE ERROR", "Error 201"); break;
                            case 202: Log.d("RESPONSE ERROR", "Error 202"); break;
                        }
                    }

                    @Override
                    public void onFailure(Call<RecipesResponse> call, Throwable t) {
                        presenter.triggerSetError(Constants.Connection_Fail);
                    }
                });
            }
        });
        thread.start();
    }
}
